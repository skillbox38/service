# Service

Requirements
------------

- [Ansible core](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) 2.11+

## Сloning a repository

```sh
$ git clone git@gitlab.com:skillbox38/service.git
$ cd service/
```
## Parameters must be set as environment variables
```
export DOMAIN="<domain_name>"
export REGISTRY="<CI_REGISTRY>"
export REGISTRY_IMAGE="<CI_REGISTRY_IMAGE>"
export REGISTRY_USER="<CI_REGISTRY_USER>"
export REGISTRY_PASSWORD="<CI_REGISTRY_PASSWORD>"
```

## Playbook launch.

```sh
$ ansible-playbook -u ansible -i 'runner.$DOMAIN,' --private-key ~/.ssh/id_rsa app.yml
```